# README

* Ruby version: 2.5.0

* Rails version: 5.1.6

This app has been created to apply my curriculum at Magnetis Investments. It is a simple app to make bank account transitions and see the current balance.

To run this app, you will need Ruby and Ruby on Rails installed in your computer. You can follow this tutorial if you need: http://guides.rubyonrails.org/getting_started.html#installing-rails

After cloning the repo into your machine, go to the application's folder and run the following commands at your command line:

- Bundle Install (to install all the gems used in this project);
- Rake db:create (to create the project's database);
- Rake db:migrate (to create the schema file and configure all the tables);

After that, feel free to start the app by running "Rails server" and enjoy :)

Since the app mailer hasn't been configured to production and neather the application it self, you should recieve the emails of account confirmation and password change in your localhost (you can see it in your command line if you're using UNIX based OS)
---

I highly recomend you to use the app in a mobile device. To do this using the localhost in your machine, you can open your phone's browser and access your machines local IP address and add the port in the end (default port is 3000).
Ex.: 192.168.0.107:3000