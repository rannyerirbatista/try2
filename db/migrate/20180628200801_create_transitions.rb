class CreateTransitions < ActiveRecord::Migration[5.1]
  def change
    create_table :transitions do |t|
      t.belongs_to :account, foreign_key: true
      t.string :value
      t.string :reciever

      t.timestamps
    end
  end
end
