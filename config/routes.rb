Rails.application.routes.draw do
  devise_for :users, :path => '', controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  authenticated :user do
    resources :deposits
    resources :transitions
    resources :accounts
  end

  resources :users
  root to: "accounts#index"
end
