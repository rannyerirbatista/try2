class Account < ApplicationRecord
  belongs_to :user
  has_many :transitions

  validates_presence_of :account_number
  validates_uniqueness_of :account_number
end
