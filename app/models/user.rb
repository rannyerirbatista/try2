class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  before_validation :check_user_email
  before_validation :generate_user_password, on: :create
  after_create :generate_user_account

  has_one :account

  validates_presence_of :name

  attr_writer :login

  def check_user_email
    self.email == self.email_confirmation ? true : errors.add(:email_confirmation, " não coincide com a confirmação do email")
  end

  def generate_user_password
    password = Devise.friendly_token.first(6)
    self.password = password
    self.password_confirmation = password
    self.unhashed_password = password
  end

  def generate_user_account
    number = Random.new.rand(1000..9999)
    number = check_account_number(number)

    Account.create(balance: "0", account_number: number.to_s + "-" + Random.new.rand(0..9).to_s, user_id: self.id)
    self.account_number = self.account.account_number
    self.save
  end

  def check_account_number(number)
    accounts = []

    Account.all.each_with_index do |ac, i|
      accounts[i] = ac.account_number
    end

    if not accounts.include?(number.to_s)
      return number
    else
      while (accounts.include?(number.to_s)) do
        number = Random.new.rand(1000..9999)
      end
      return number
    end
  end

  # devise login

  def login=(login)
    @login = login
  end

  def login
    @login || self.account_number || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(account_number) = :value OR lower(email) = :value", { :value => login.downcase  }]).first
    elsif conditions.has_key?(:account_number) || conditions.has_key?(:email)
      where(conditions.to_hash).first
    end
  end
end
