class Transition < ApplicationRecord
  belongs_to :account

  validates_presence_of :account
  validates_presence_of :value
  validates_presence_of :reciever
end
