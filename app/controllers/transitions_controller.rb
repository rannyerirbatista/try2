class TransitionsController < ApplicationController

  # GET /accounts
  # GET /accounts.json
  def index
    @transitions = Transition.all
    @transitions += Deposit.where(account: current_user.account)
  end

  # GET /accounts/new
  def new
    @transition = Transition.new
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @transition = Transition.new(transition_params)

    reciever = Account.find_by_account_number(transition_params[:reciever])

    if not reciever
      respond_to do |format|
        format.html { render :new  }
        flash[:danger] = "Nenhuma conta com o número digitado foi encontrada, verifique sua digitação e tente novamente"
      end
    else
      reciever.balance = reciever.balance.to_f + transition_params[:value].to_f
      reciever.save

      sender = Account.find_by_id(transition_params[:account_id])
      sender.balance = sender.balance.to_f - transition_params[:value].to_f
      sender.save

      respond_to do |format|
        if @transition.save
          format.html { redirect_to transitions_path }
          flash[:success] = "Transferência realizada com sucesso!"
          format.json { render :show, status: :created, location: @transition }
        else
          format.html { render :new }
          format.json { render json: @transition.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def transition_params
      params.require(:transition).permit(:value, :reciever, :account_id)
    end
end
